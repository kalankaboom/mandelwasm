document.addEventListener("keydown", (event) => {
    if (event.key === "Escape") {
        document.activeElement.blur();
    }
    const ui = document.getElementById("uiwrapper");
    if (ui.contains(document.activeElement)) {
        if (event.key === "Enter") {
            document.activeElement.blur();
            _reRender();
        }
        return;
    }
    switch (event.key) {
        case "ArrowDown":
        case "s":
            _goDown();
            break;
        case "ArrowUp":
        case "w":
            _goUp();
            break;
        case "ArrowLeft":
        case "a":
            _goLeft();
            break;
        case "ArrowRight":
        case "d":
            _goRight();
            break;
        case "-":
            _zoomOut();
            break;
        case "+":
            _zoomIn();
            break;
        case "r":
            _resetZoom();
            break;
        case "h":
            if (ui.style.opacity === "") {
                ui.style.opacity = 0;
                break;
            }
            ui.style.opacity = Math.abs(parseInt(ui.style.opacity) - 1);
            break;
        default:
            return;
    }
});

document.getElementById("uiwrapper").addEventListener("click", (event) => {
    if (event.target === event.currentTarget) {
        document.activeElement.blur();
    }
});

function hexToRgb(hex) {
    return [
        parseInt(hex.substring(1, 3), 16),
        parseInt(hex.substring(3, 5), 16),
        parseInt(hex.substring(5), 16),
    ];
}

function populateColours() {
    const picker = document.getElementById("colour_picker");
    const colours = [
        "#3f001d",
        "#1e000e",
        "#09012f",
        "#040449",
        "#000764",
        "#0c2c8a",
        "#1852b1",
        "#397dd1",
        "#86b5e5",
        "#d3ecf8",
        "#f1e9bf",
        "#f8c95f",
        "#ffaa00",
        "#cc8000",
        "#995700",
        "#6a3403",
    ];

    for (let k = 0; k < colours.length; k++) {
        const wrap = document.createElement("div");
        const id = "colour" + k;
        const input = document.createElement("input");
        input.setAttribute("type", "color");
        input.setAttribute("name", id);
        input.setAttribute("id", id);
        input.setAttribute("value", colours[k]);
        input.addEventListener("change", (event) => {
            _setColour(k, ...hexToRgb(event.target.value));
        });
        wrap.appendChild(input);
        picker.appendChild(wrap);
    }
}
