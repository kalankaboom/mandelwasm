#include <algorithm>
#include <array>
#include <cmath>
#include <emscripten/emscripten.h>
#include <iostream>

#include "State.hpp"

constexpr double DEFAULT_BAILOUT = 4;
constexpr double DEFAULT_BLUR_BAILOUT = 1U << 16U;
constexpr double DEFAULT_JULIA_BLUR_BAILOUT = 1U << 16U;
constexpr double MOVE_BY_FRAC = 0.1;
constexpr unsigned int DEFAULT_FPS = 10;
constexpr unsigned int DEFAULT_HEIGHT = 1080;
constexpr unsigned int DEFAULT_MAX_ITERATIONS = 1000;
constexpr unsigned int DEFAULT_WIDTH = 1080;
constexpr double DEFAULT_ZOOM = 0.2;
const std::array<Rgb, NB_COLOURS> DEFAULT_COLOURS = {{{63, 0, 29},
                                                      {30, 0, 14},
                                                      {9, 1, 47},
                                                      {4, 4, 73},
                                                      {0, 7, 100},
                                                      {12, 44, 138},
                                                      {24, 82, 177},
                                                      {57, 125, 209},
                                                      {134, 181, 229},
                                                      {211, 236, 248},
                                                      {241, 233, 191},
                                                      {248, 201, 95},
                                                      {255, 170, 0},
                                                      {204, 128, 0},
                                                      {153, 87, 0},
                                                      {106, 52, 3}}};

using namespace std::chrono_literals;

State State(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_MAX_ITERATIONS, DEFAULT_ZOOM,
            {0, 0}, {0, 0}, DEFAULT_FPS, DEFAULT_COLOURS);

[[nodiscard]]
auto lerp(Rgb col1, const Rgb &col2, const double smooth_it) -> Rgb {
    std::transform(col1.begin(), col1.end(), col2.begin(), col1.begin(),
                   [smooth_it](const double val1, const double val2) {
                       return val1 + smooth_it * (val2 - val1);
                   });
    return col1;
}

void mandel(const unsigned int max_iterations = State.getMaxIterations()) {
    State.clear();
    const unsigned int width = State.getWidth();
    const unsigned int height = State.getHeight();
    const double scale = 1 / (State.getZoom() * static_cast<double>(width));
    const Vec2 centre = {
        -State.getX() + (static_cast<double>(width) / 2.0) * scale,
        -State.getY() + (static_cast<double>(height) / 2.0) * scale};

    Vec2 cmp_z = {0, 0};
    Vec2 cmp_c = {0, 0};
    double sqx = 0;
    double sqy = 0;
    unsigned int iter = 0;

    for (unsigned int h_inc = 0; h_inc < height; h_inc++) {
        for (unsigned int w_inc = 0; w_inc < width; w_inc++) {
            cmp_c = {static_cast<double>(w_inc) * scale - centre[0],
                     static_cast<double>(h_inc) * scale - centre[1]};

            cmp_z = {0, 0};
            sqx = 0;
            sqy = 0;
            iter = 0;

            while (iter < max_iterations && sqx + sqy < DEFAULT_BAILOUT) {
                iter++;
                cmp_z[1] = 2 * cmp_z[0] * cmp_z[1] + cmp_c[1];
                cmp_z[0] = sqx - sqy + cmp_c[0];
                sqx = std::pow(cmp_z[0], 2.0);
                sqy = std::pow(cmp_z[1], 2.0);
            }

            if (iter == max_iterations) {
                State.colourPixel(static_cast<int>(w_inc),
                                  static_cast<int>(h_inc), {0, 0, 0});
                continue;
            }

            State.colourPixel(static_cast<int>(w_inc), static_cast<int>(h_inc),
                              State.getModColour(iter));
        }
    }
    State.render();
}

// WARNING duplicate code for speed

void julia(const unsigned int max_iterations = State.getMaxIterations()) {
    State.clear();
    const unsigned int width = State.getWidth();
    const unsigned int height = State.getHeight();
    const double scale = 1 / (State.getZoom() * static_cast<double>(width));
    const Vec2 centre = {
        -State.getX() + (static_cast<double>(width) / 2.0) * scale,
        -State.getY() + (static_cast<double>(height) / 2.0) * scale};

    Vec2 cmp_z = {0, 0};
    const Vec2 cmp_c = {State.getJuliaX(), State.getJuliaY()};
    double sqx = 0;
    double sqy = 0;
    unsigned int iter = 0;

    for (unsigned int h_inc = 0; h_inc < height; h_inc++) {
        for (unsigned int w_inc = 0; w_inc < width; w_inc++) {
            cmp_z = {static_cast<double>(w_inc) * scale - centre[0],
                     static_cast<double>(h_inc) * scale - centre[1]};

            sqx = std::pow(cmp_z[0], 2.0);
            sqy = std::pow(cmp_z[1], 2.0);
            iter = 0;

            while (iter < max_iterations && sqx + sqy < DEFAULT_BAILOUT) {
                iter++;
                cmp_z[1] = 2 * cmp_z[0] * cmp_z[1] + cmp_c[1];
                cmp_z[0] = sqx - sqy + cmp_c[0];
                sqx = std::pow(cmp_z[0], 2.0);
                sqy = std::pow(cmp_z[1], 2.0);
            }

            if (iter == max_iterations) {
                State.colourPixel(static_cast<int>(w_inc),
                                  static_cast<int>(h_inc), {0, 0, 0});
                continue;
            }

            State.colourPixel(static_cast<int>(w_inc), static_cast<int>(h_inc),
                              State.getModColour(iter));
        }
    }
    State.render();
}

void blurryMandel(
    const unsigned int max_iterations = State.getMaxIterations()) {
    State.clear();
    const unsigned int width = State.getWidth();
    const unsigned int height = State.getHeight();
    const double scale = 1 / (State.getZoom() * static_cast<double>(width));
    const Vec2 centre = {
        -State.getX() + (static_cast<double>(width) / 2.0) * scale,
        -State.getY() + (static_cast<double>(height) / 2.0) * scale};

    Vec2 cmp_z = {0, 0};
    Vec2 cmp_c = {0, 0};
    double sqx = 0;
    double sqy = 0;
    unsigned int iter = 0;

    for (unsigned int h_inc = 0; h_inc < height; h_inc++) {
        for (unsigned int w_inc = 0; w_inc < width; w_inc++) {
            cmp_c = {static_cast<double>(w_inc) * scale - centre[0],
                     static_cast<double>(h_inc) * scale - centre[1]};

            cmp_z = {0, 0};
            sqx = 0;
            sqy = 0;
            iter = 0;

            while (iter < max_iterations && sqx + sqy < DEFAULT_BLUR_BAILOUT) {
                iter++;
                cmp_z[1] = 2 * cmp_z[0] * cmp_z[1] + cmp_c[1];
                cmp_z[0] = sqx - sqy + cmp_c[0];
                sqx = std::pow(cmp_z[0], 2.0);
                sqy = std::pow(cmp_z[1], 2.0);
            }

            if (iter == max_iterations) {
                State.colourPixel(static_cast<int>(w_inc),
                                  static_cast<int>(h_inc), {0, 0, 0});
                continue;
            }

            const double blurmod = std::log2(std::log2(sqx + sqy)) - 1;

            const double res = (iter + 1) - blurmod;
            const unsigned int new_n = floor(res);

            State.colourPixel(static_cast<int>(w_inc), static_cast<int>(h_inc),
                              lerp(State.getModColour(new_n),
                                   State.getModColour(new_n + 1), res - new_n));
        }
    }
    State.render();
}

void blurryJulia(const unsigned int max_iterations = State.getMaxIterations()) {
    State.clear();
    const unsigned int width = State.getWidth();
    const unsigned int height = State.getHeight();
    const double scale = 1 / (State.getZoom() * static_cast<double>(width));
    const Vec2 centre = {
        -State.getX() + (static_cast<double>(width) / 2.0) * scale,
        -State.getY() + (static_cast<double>(height) / 2.0) * scale};

    Vec2 cmp_z = {0, 0};
    const Vec2 cmp_c = {State.getJuliaX(), State.getJuliaY()};
    double sqx = 0;
    double sqy = 0;
    unsigned int iter = 0;

    for (unsigned int h_inc = 0; h_inc < height; h_inc++) {
        for (unsigned int w_inc = 0; w_inc < width; w_inc++) {
            cmp_z = {static_cast<double>(w_inc) * scale - centre[0],
                     static_cast<double>(h_inc) * scale - centre[1]};

            sqx = std::pow(cmp_z[0], 2.0);
            sqy = std::pow(cmp_z[1], 2.0);
            iter = 0;

            while (iter < max_iterations &&
                   sqx + sqy < DEFAULT_JULIA_BLUR_BAILOUT) {
                iter++;
                cmp_z[1] = 2 * cmp_z[0] * cmp_z[1] + cmp_c[1];
                cmp_z[0] = sqx - sqy + cmp_c[0];
                sqx = std::pow(cmp_z[0], 2.0);
                sqy = std::pow(cmp_z[1], 2.0);
            }

            if (iter == max_iterations) {
                State.colourPixel(static_cast<int>(w_inc),
                                  static_cast<int>(h_inc), {0, 0, 0});
                continue;
            }

            const double blurmod = std::log2(std::log2(sqx + sqy)) - 1;

            const double res = (iter + 1) - blurmod;
            const unsigned int new_n = floor(res);

            State.colourPixel(static_cast<int>(w_inc), static_cast<int>(h_inc),
                              lerp(State.getModColour(new_n),
                                   State.getModColour(new_n + 1), res - new_n));
        }
    }
    State.render();
}

void animateIterationsMandel(void *cur_ptr) {
    auto *current = static_cast<unsigned int *>(cur_ptr);
    const unsigned int current_val = *current;
    if (current_val > State.getMaxIterations()) {
        emscripten_cancel_main_loop();
        State::saveAsPNG();
        return;
    }
    if (State.chooseJulia()) {
        if (State.isBlurry()) {
            blurryJulia(current_val);
        } else {
            julia(current_val);
        }
    } else {
        if (State.isBlurry()) {
            blurryMandel(current_val);
        } else {
            mandel(current_val);
        }
    }
    ++*current;
}

extern "C" {
void changeSize(const unsigned int width, const unsigned int height) {
    emscripten_cancel_main_loop();
    State.changeSize(width, height);
    State.clearToBg();
}
void reRender() {
    if (State.animateIterations()) {
        emscripten_cancel_main_loop();
        unsigned int current = 0;
        void *cur_ptr = &current;
        emscripten_set_main_loop_arg(animateIterationsMandel, cur_ptr,
                                     (int)State.getFPS(), 1);

        return;
    }
    if (State.chooseJulia()) {
        if (State.isBlurry()) {
            blurryJulia();
        } else {
            julia();
        }
    } else {
        if (State.isBlurry()) {
            blurryMandel();
        } else {
            mandel();
        }
    }

    if (State.shouldSave()) { State::saveAsPNG(); }
}
void goUp() {
    State.setY(State.getY() - MOVE_BY_FRAC / State.getZoom());
    State.updateUI();
    reRender();
}
void goDown() {
    State.setY(State.getY() + MOVE_BY_FRAC / State.getZoom());
    State.updateUI();
    reRender();
}
void goRight() {
    State.setX(State.getX() + MOVE_BY_FRAC / State.getZoom());
    State.updateUI();
    reRender();
}
void goLeft() {
    State.setX(State.getX() - MOVE_BY_FRAC / State.getZoom());
    State.updateUI();
    reRender();
}
void zoomIn() {
    State.scaleZoom(2);
    State.updateUI();
    reRender();
}
void zoomOut() {
    State.scaleZoom(0.5);
    State.updateUI();
    reRender();
}
void resetZoom() {
    State.setZoom(DEFAULT_ZOOM);
    State.updateUI();
    reRender();
}
void setZoom(const double new_zoom) { State.setZoom(new_zoom); }
void toggleJulia() { State.toggleJulia(); }
void toggleBlur() { State.toggleBlur(); }
void toggleSave() { State.toggleSave(); }
void toggleAnimateIterations() {
    emscripten_cancel_main_loop();
    State.toggleAnimateIterations();
}
void setMaxIterations(const unsigned int max) { State.setMaxIterations(max); }
void setX(const double new_x) { State.setX(new_x); }
void setY(const double new_y) { State.setY(new_y); }
void setJuliaX(const double new_x) { State.setJuliaX(new_x); }
void setJuliaY(const double new_y) { State.setJuliaY(new_y); }
void setFPS(const unsigned int fps) { State.setFPS(fps); }
void setColour(const unsigned int ind, const uint8_t red, const uint8_t green,
               const uint8_t blue) {
    State.setColour(ind, red, green, blue);
}
}

auto main() -> int {
    State.initSDL();
    SDL_EventState(SDL_TEXTINPUT, SDL_DISABLE);
    SDL_EventState(SDL_KEYDOWN, SDL_DISABLE);
    SDL_EventState(SDL_KEYUP, SDL_DISABLE);
    SDL_EventState(SDL_MOUSEMOTION, SDL_DISABLE);
    SDL_EventState(SDL_MOUSEBUTTONUP, SDL_DISABLE);
    SDL_EventState(SDL_MOUSEBUTTONDOWN, SDL_DISABLE);

    mandel();

    return 0;
}
