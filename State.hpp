#pragma once
#include <SDL2/SDL.h>
#include <array>
#include <emscripten.h>
#include <emscripten/em_asm.h>

constexpr unsigned int NB_COLOURS = 16;
constexpr double MAX_ZOOM = 14073748835532.8;

using Vec2 = std::array<double, 2>;
using Rgb = std::array<uint8_t, 3>;

class State {
  public:
    State(const unsigned int width, const unsigned int height,
          const unsigned int max_iterations, const double zoom,
          const Vec2 &point, const Vec2 &julia_point, const unsigned int fps,
          const std::array<Rgb, NB_COLOURS> &colours)
        : width(width), height(height), max_iterations(max_iterations),
          zoom(zoom), point(point), julia_point(julia_point), fps(fps),
          colours(colours) {}
    State(State &&) = delete;
    State(const State &) = delete;
    auto operator=(State &&) -> State & = delete;
    auto operator=(const State &) -> State & = delete;
    ~State() = default;

    void scaleZoom(const double scale) {
        zoom *= scale;
        if (zoom > MAX_ZOOM) { zoom = MAX_ZOOM; }
    }
    void setZoom(const double new_zoom) {
        if (new_zoom < MAX_ZOOM && new_zoom != 0.0) { zoom = new_zoom; }
    }
    void setX(const double new_x) { point[0] = new_x; }
    void setY(const double new_y) { point[1] = new_y; }
    void setJuliaX(const double new_x) { julia_point[0] = new_x; }
    void setJuliaY(const double new_y) { julia_point[1] = new_y; }
    void changeSize(const unsigned int new_w, const unsigned int new_h) {
        width = (new_w <= 0) ? 1 : new_w;
        height = (new_h <= 0) ? 1 : new_h;

        SDL_SetWindowSize(window, static_cast<int>(width),
                          static_cast<int>(height));
    }
    void setMaxIterations(const unsigned int new_max) {
        max_iterations = new_max;
    }
    void setFPS(const unsigned int new_fps) { fps = new_fps; }
    void setColour(const unsigned int ind, const uint8_t red,
                   const uint8_t green, const uint8_t blue) {
        colours[ind] = {red, green, blue};
    }
    void initSDL() {
        SDL_Init(SDL_INIT_VIDEO);
        SDL_CreateWindowAndRenderer(static_cast<int>(width),
                                    static_cast<int>(height), 0, &window,
                                    &renderer);
    }
    void toggleJulia() { julia = !julia; }
    void toggleBlur() { blurry = !blurry; }
    void toggleSave() { save = !save; }
    void toggleAnimateIterations() { animate_iter = !animate_iter; }

    inline void colourPixel(const int w_pos, const int h_pos,
                            const Rgb &colour) const {
        SDL_SetRenderDrawColor(renderer, colour[0], colour[1], colour[2], 1);
        SDL_RenderDrawPoint(renderer, w_pos, h_pos);
    }
    void render() const { SDL_RenderPresent(renderer); }
    void clear() const { SDL_RenderClear(renderer); }
    void clearToBg() const {
        SDL_SetRenderDrawColor(renderer, 29, 48, 48, 1);
        SDL_RenderClear(renderer);
        SDL_RenderPresent(renderer);
    }
    static void saveAsPNG() {
        EM_ASM(const save = document.createElement("a");
               save.download = "mandel.png";
               save.href = document.getElementById("canvas").toDataURL();
               save.click(););
    }
    void updateUI() const {
        EM_ASM(
            {
                document.getElementById("real_input").value = $0;
                document.getElementById("imag_input").value = $1;
                document.getElementById("zoom_input").value = $2;
            },
            point[0], point[1], zoom);
    }

    [[nodiscard]]
    auto getWidth() const -> unsigned int {
        return width;
    }
    [[nodiscard]]
    auto getHeight() const -> unsigned int {
        return height;
    }
    [[nodiscard]]
    auto getZoom() const -> double {
        return zoom;
    }
    [[nodiscard]]
    auto getX() const -> double {
        return point[0];
    }
    [[nodiscard]]
    auto getY() const -> double {
        return point[1];
    }
    [[nodiscard]]
    auto getJuliaX() const -> double {
        return julia_point[0];
    }
    [[nodiscard]]
    auto getJuliaY() const -> double {
        return julia_point[1];
    }
    [[nodiscard]]
    auto getMaxIterations() const -> unsigned int {
        return max_iterations;
    }
    [[nodiscard]]
    auto getFPS() const -> unsigned int {
        return fps;
    }
    [[nodiscard]]
    auto chooseJulia() const -> bool {
        return julia;
    }
    [[nodiscard]]
    auto isBlurry() const -> bool {
        return blurry;
    }
    [[nodiscard]]
    auto shouldSave() const -> bool {
        return save;
    }
    [[nodiscard]]
    auto animateIterations() const -> bool {
        return animate_iter;
    }
    [[nodiscard]]
    auto getModColour(const unsigned int mod_ind) const -> const Rgb & {
        return colours[mod_ind % colours.size()];
    }

  private:
    unsigned int width;
    unsigned int height;
    unsigned int max_iterations;
    double zoom;
    Vec2 point;
    Vec2 julia_point;
    unsigned int fps;
    bool julia{};
    bool blurry{};
    bool save{};
    bool animate_iter{};
    std::array<Rgb, NB_COLOURS> colours;
    SDL_Window *window{};
    SDL_Renderer *renderer{};
};
