CC = emcc
all: mandelwasm.cpp
	$(CC) mandelwasm.cpp -O2 -s ALLOW_MEMORY_GROWTH=1 -s EXPORTED_FUNCTIONS="_main,_changeSize,_reRender,_goUp,_goDown,_goRight,_goLeft,_zoomIn,_zoomOut,_resetZoom,_setZoom,_toggleJulia,_toggleBlur,_toggleSave,_toggleAnimateIterations,_setMaxIterations,_setX,_setY,_setJuliaX,_setJuliaY,_setFPS,_setColour" -s USE_SDL=2 -s USE_SDL_IMAGE=2 -o mandelwasm.js
